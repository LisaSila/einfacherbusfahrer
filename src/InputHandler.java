import model.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {

    private Model model = new Model();

    public InputHandler(Model model) { this.model = model; }

    public void onKeyPressed(KeyCode keyCode){
        if (keyCode == KeyCode.H) {
            model.playGame(1);
        } else if (keyCode == KeyCode.L) {
            model.playGame(0);
        } else if (keyCode == KeyCode.I) {
            model.playGame(2);
        } else if (keyCode == KeyCode.ENTER){
            model.resetInGame();
        } else if (keyCode == KeyCode.ESCAPE){
            model.closeGame();
        }
    }

}
