import graphics.Graphics;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import model.Model;

public class Main extends Application {

        private Timer timer;

        @Override
        public void start(Stage stage) throws Exception {

            stage.setTitle("Busfahrer Spiel Version 1.0");

            Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);

            Group group = new Group();
            group.getChildren().add(canvas);
            Scene scene = new Scene(group);
            stage.setScene(scene);
            stage.show();

            GraphicsContext gc = canvas.getGraphicsContext2D();

            Model model = new Model();
            Graphics graphics = new Graphics(gc, model);

            timer = new Timer(graphics,model);
            timer.start();

            InputHandler inputHandler = new InputHandler(model);

            scene.setOnKeyPressed(
                    event -> { inputHandler.onKeyPressed(event.getCode()); }
            );

        }

}

