import graphics.Graphics;
import javafx.animation.AnimationTimer;
import model.Model;

public class Timer extends AnimationTimer {

    private Graphics graphics;
    private Model model;

    public Timer(Graphics graphics, Model model) {
        this.graphics = graphics;
        this.model = model;
    }

    @Override
    public void handle(long nowNano) {
         graphics.drawMainScreen();

         //TODO: Bug, wenn im Endscreen weiter Ratetasten und Enter drückt kommt man zurück
        if (model.getWinCount() == 4){
                graphics.drawEnd();
        }

    }
}
