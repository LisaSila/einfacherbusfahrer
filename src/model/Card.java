package model;

public class Card {

    private int id;
    private int rank;
    private int suit;

    public Card(int id, int rank, int suit) {
        this.id = id;
        this.rank = rank;
        this.suit = suit;
    }

    public int getId() {
        return id;
    }

    public int getRank() {
        return rank;
    }

    public int getSuit() {
        return suit;
    }

}
