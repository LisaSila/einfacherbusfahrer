package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Model {

    public static final int WIDTH = 700;
    public static final int HEIGHT = 700;

    private List<Card> deck;
    private int roundCount = 0;
    private int winCount = 0;

    public int getWinCount() {
        return winCount;
    }

    public Model() {
        deck = new ArrayList<>();
        int id = 0;
        for (int suit = 0; suit < 4; suit++) {
            for (int rank = 0; rank < 8; rank++) {
                deck.add(new Card(id, rank, suit));
                id++;
            }
        }
        Collections.shuffle(deck);
    }

    public Card getCurrentCard() {
        return deck.get(0);
    }

    public Card getNextCard() {
        return deck.get(1);
    }

    public String showResult() {
        switch (roundCount) {
            case 1:
                return "You have chosen wisely"; //win
            case 2:
                return "You have chosen poorly"; //lost
        }
        return "How will you choose?";
    }

    private boolean gameStatus = true;

    //TODO: evtl. Suit mit einbeziehen?
    //TODO: aus dem Kartenarray, die bereits ausgespielten Karten entfernen
    public void playGame(int userInput) {

        while (gameStatus) {
            if (userInput == 1
                    && (getCurrentCard().getRank() < getNextCard().getRank())) { //higher
                choseCorrectly();
                return;
            } else if (userInput == 0
                    && (getCurrentCard().getRank() > getNextCard().getRank())) { //lower
                choseCorrectly();
                return;
            } else if (userInput == 2
                    && (getCurrentCard().getRank() == getNextCard().getRank())) { //identical
                choseCorrectly();
                return;
            } else {
                roundCount = 2;
                winCount = 0;
                gameStatus = false;
                System.out.println("Falsch geraten.");
                return;
            }
        }
    }

    /**
     * What happens if the User chose correctly
     * <p>
     * roundCount: as Case 1 in showResult-method
     * winCount: increases the User-Score
     * gameStatus: set false to break the while loop in playGame-method
     */
    private void choseCorrectly() {
        roundCount = 1;
        winCount++;
        gameStatus = false;
        System.out.println("Richtig geraten.");
    }

    public void resetInGame() {
        System.out.println("Ich drücke Enter");
        gameStatus = true;
        Collections.shuffle(deck);
        roundCount = 0;
    }

    public void startNewGame(){
        //TODO: Methode schreiben um aus dem Endbildschirm ein neues Spiel zu starten, aber gleichzeitig bekannten Bug sperren
    }


    public void closeGame() {
        System.exit(0);
    }

}
