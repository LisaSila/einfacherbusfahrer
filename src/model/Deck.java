package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {

    private List<Card> deck;

    public Deck() {
        deck = new ArrayList<>();
        int id = 0;
        for (int suit = 1; suit < 3; suit++) {
            for (int rank = 0; rank < 8; rank++) {
                deck.add(new Card(id, rank, suit));
                id++;
            }
        }
    }

    public List<Card> getDeck(int i) {
        return (List<Card>) deck.get(i);
    }

    private void shuffle() {
        Collections.shuffle(deck);
    }

}
