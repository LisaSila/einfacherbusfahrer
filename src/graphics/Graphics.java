package graphics;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import model.Card;
import model.Model;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Graphics {

    private GraphicsContext gc;
    private Model model;

    private Image imgBackground;
    private Image picLogo;
    private Image endBackground;
    private Image[] picCards = new Image[32]; // 32 cards

    public Graphics(GraphicsContext gc, Model model) throws FileNotFoundException {
        this.gc = gc;
        this.model = model;
        try {
            this.imgBackground = new Image(new FileInputStream("src/graphics/pics/wood_Background.jpg"));
            this.picLogo= new Image(new FileInputStream("src/graphics/pics/all_four.png"));
            this.endBackground = new Image(new FileInputStream("src/graphics/pics/_win_pic_2.jpg"));
            this.picCards = new Image[]{
                    new Image(new FileInputStream("src/graphics/pics/clubs/7_clubs.jpg")), // index 0
                    new Image(new FileInputStream("src/graphics/pics/clubs/8_clubs.jpg")), //index 1
                    new Image(new FileInputStream("src/graphics/pics/clubs/9_clubs.jpg")), //index 2
                    new Image(new FileInputStream("src/graphics/pics/clubs/U_clubs.jpg")), //index 3
                    new Image(new FileInputStream("src/graphics/pics/clubs/O_clubs.jpg")), //index 4
                    new Image(new FileInputStream("src/graphics/pics/clubs/K_clubs.jpg")), //index 5
                    new Image(new FileInputStream("src/graphics/pics/clubs/10_clubs.jpg")), //index 6
                    new Image(new FileInputStream("src/graphics/pics/clubs/A_clubs.jpg")), // index 7
                    new Image(new FileInputStream("src/graphics/pics/leaf/7_leaf.jpg")), // index 8
                    new Image(new FileInputStream("src/graphics/pics/leaf/8_leaf.jpg")), // index 9
                    new Image(new FileInputStream("src/graphics/pics/leaf/9_leaf.jpg")), // index 10
                    new Image(new FileInputStream("src/graphics/pics/leaf/U_leaf.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/leaf/O_leaf.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/leaf/K_leaf.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/leaf/10_leaf.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/leaf/A_leaf.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/bell/7_bell.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/bell/8_bell.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/bell/9_bell.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/bell/U_bell.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/bell/O_bell.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/bell/K_bell.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/bell/10_bell.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/bell/A_bell.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/heart/7_card_heart.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/heart/8_card_heart.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/heart/9_card_heart.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/heart/U_card_heart.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/heart/O_card_heart.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/heart/K_card_heart.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/heart/10_card_heart.jpg")), //
                    new Image(new FileInputStream("src/graphics/pics/heart/ace_card_heart.jpg")) //
            };
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            //TODO: z B. Nutzermeldung
        }
    }

    public void drawStartScreen(){
        //TODO: erste Maske, entscheidet, ob jungenfreie Version oder Trinkspiel
        //TODO: mit kurzer Erklärung Spiel
    }

    public void drawMainScreen() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        gc.drawImage(imgBackground, 0, 0);

        gc.drawImage(picLogo, 610,10);

        gc.setFont(Font.font("Comic Sans S", 20));
        gc.setFill(Color.SNOW);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText("Score: " + model.getWinCount(), 50, 50);

        gc.setFont(Font.font("Comic Sans S", 40));
        gc.setFill(Color.SNOW);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText(model.showResult(), 350, 120);

        Card card1 = model.getCurrentCard();
        gc.drawImage(picCards[card1.getId()], 180, 200);

        //TODO: kann man machen, aber besser ned; lieber boolean win oder so
        if (model.showResult().equals("You have chosen wisely") || model.showResult().equals("You have chosen poorly")) {
            Card card2 = model.getNextCard();
            gc.drawImage(picCards[card2.getId()], 380, 200);
        }

        gc.setFont(Font.font("Comic Sans S", FontWeight.SEMI_BOLD, FontPosture.REGULAR, 25));
        gc.setFill(Color.SNOW);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText("H = higher \n L = lower \n Enter = next round\n esc = exit game", 350, 520);

    }

    public void drawEnd() {

        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        gc.drawImage(endBackground, 0, 0);

        gc.setFont(Font.font("Comic Sans S", 40));
        gc.setFill(Color.SNOW);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText("Congrats!\n \n You won 4 times in a row!", 350, 250);

        gc.setFont(Font.font("Comic Sans S", 20));
        gc.setFill(Color.SNOW);
        gc.fillText("press esc to exit game", 120, 50);

    }

}
