## Busfahrer - the game

"Busfahrer" is a card based drinking game in Bavaria/Germany. 
In the rest of the world also known as "higher or lower".

During the Academic Work Academy in 2019 we were told to programme a simple game using Javafx.
It was my first time using JavaFX, and I just started learning Java. 
At that time it was the 4th week of training in the bootcamp.

I used very simple methods at that time and would definitely code this game differently today.
Anyway it was still fun to make.

**start screen**

![startscreen](pictures/BusfahrerStartGame.PNG)

The score is counting the number of right guesses.

**chosen correctly**

![in game](pictures/BusfahrerChosenWisely.PNG)

**winning screen**

The player wins the game, if he guessed 4 times in a row correctly.

![win](pictures/BusfahrerFourTimesInARow.PNG)

